package com.englishdom.util;

import com.englishdom.webdriver.WebDriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class WaitUtils {
    private static final WebDriverWait wait = new WebDriverWait(
            WebDriverFactory.getCurrentDriver(), 5, 250);

    public static void waitForAttributeValue(
            final WebElement elementToWait,
            final String attribute,
            final String valueToWait) {
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(final WebDriver webDriver) {
                String currentValue = elementToWait.getAttribute(attribute);
                return currentValue.contains(valueToWait);
            }
        });
    }

    public static void waitForAttributeValueToDisappear(
            final WebElement elementToWait,
            final String attribute,
            final String valueToWait) {
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(final WebDriver webDriver) {
                String currentValue = elementToWait.getAttribute(attribute);
                return !currentValue.contains(valueToWait);
            }
        });
    }

    /**
     * Waits for element to have the same attribute value for at least 3 polls.
     * Polling time is defined in wait field.
     */
    public static void waitForAttributeToStopChanging(
            final WebElement elementToWait,
            final String attribute) {
        wait.until(new ExpectedCondition<Boolean>() {
            String previousValue = null;
            int numberOfIterationsNotChanged = 0;

            @Override
            public Boolean apply(final WebDriver webDriver) {

                String currentValue = elementToWait.getAttribute(attribute);

                if (previousValue == null) {
                    previousValue = currentValue;
                    return false;
                }

                if (!currentValue.equals(previousValue)) {
                    numberOfIterationsNotChanged = 0;
                    previousValue = currentValue;
                    return false;
                } else if (currentValue.equals(previousValue) && numberOfIterationsNotChanged < 2) {
                    numberOfIterationsNotChanged++;
                    previousValue = currentValue;
                    return false;
                } else {
                    return numberOfIterationsNotChanged >= 2;
                }
            }
        });
    }

    public static void waitForAllElementsLoaded(
            final String cssSelector,
            final int numberOfElementsToWait) {
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(final WebDriver webDriver) {
                return webDriver.findElements(By
                        .cssSelector(cssSelector)).size() == numberOfElementsToWait;
            }
        });
    }


    public static void waitForJQueryToLoad(final WebDriver webDriver) {
        wait.until(new ExpectedCondition<Boolean>() {
                       public Boolean apply(WebDriver driver) {
                           return (Boolean) ((JavascriptExecutor) driver)
                                   .executeScript("return jQuery.active == 0;");
                       }
                   }
        );
    }

    public static void switchOffImplicitWait(WebDriver webDriver) {
        webDriver.manage().timeouts().implicitlyWait(250, TimeUnit.MILLISECONDS);
    }

    public static void switchOnImplicitWait(WebDriver webDriver) {
        long implicitWait = Long.parseLong(PropertyLoader.loadProperty("wait.implicit"));
        webDriver.manage().timeouts().implicitlyWait(implicitWait, TimeUnit.SECONDS);
    }
}
