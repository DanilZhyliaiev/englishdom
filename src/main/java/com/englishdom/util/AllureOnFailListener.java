package com.englishdom.util;

import com.englishdom.webdriver.WebDriverFactory;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import ru.yandex.qatools.allure.annotations.Attachment;

public class AllureOnFailListener implements ITestListener {
    @Override
    public void onTestStart(final ITestResult result) { }

    @Override
    public void onTestSuccess(final ITestResult result) { }

    @Override
    public void onTestFailure(final ITestResult result) {
        takeAllureScreenshot(result.getName());
    }

    @Override
    public void onTestSkipped(final ITestResult result) { }

    @Override
    public void onTestFailedButWithinSuccessPercentage(final ITestResult result) {
        takeAllureScreenshot(result.getName());
    }

    @Override
    public void onStart(final ITestContext context) { }

    @Override
    public void onFinish(final ITestContext context) { }

    @Attachment(value = "{0}", type = "image/png")
    private static byte[] takeAllureScreenshot(String name) {
        return ((TakesScreenshot) WebDriverFactory.getCurrentDriver())
                .getScreenshotAs(OutputType.BYTES);
    }
}