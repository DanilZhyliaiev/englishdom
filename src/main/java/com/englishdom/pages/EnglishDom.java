package com.englishdom.pages;

import com.englishdom.pages.blog.BlogPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Facade for all site parts and pages. E.g. sidebar, footer, blog catalogue page,
 * blog article page etc.
 */
public class EnglishDom {
    private WebDriver webDriver;
    BlogPage blogPage;

    public EnglishDom(final WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public BlogPage getBlogPage() {
        if (blogPage == null) {
            blogPage = PageFactory.initElements(webDriver, BlogPage.class);
        }

        return blogPage;
    }
}
