package com.englishdom.pages.blog;

import com.englishdom.uimaps.blog.BlogPageUIMap;
import org.openqa.selenium.WebDriver;
import com.englishdom.pages.Page;
import org.openqa.selenium.support.PageFactory;

import java.util.HashSet;
import java.util.List;

public class BlogPage extends Page {
    private BlogPageUIMap controls;


    public BlogPage(final WebDriver webDriver) {
        super(webDriver);
        controls = PageFactory.initElements(webDriver, BlogPageUIMap.class);
    }

    public BlogPageUIMap getControls() {
        return controls;
    }

    public List<BlogPageUIMap.BlogItem> getBlogItems() {
        return controls.getBlogItems();
    }

    //    Filters actions
    public BlogPage filterByDate() {
        controls.getDateFilter().click();

        controls.waitForPageLoad();

        return this;
    }

    public BlogPage filterByRating() {
        controls.getRatingFilter().click();

        controls.waitForPageLoad();

        return this;
    }

    public BlogPage filterByComments() {
        controls.getCommentsFilter().click();

        controls.waitForPageLoad();

        return this;
    }

//    Search actions
    public BlogPage openSearchForm() {
        if (!controls.getSearchForm().isActive()) {
            controls.getSearchForm().getOpenSearchFormButton().click();

            controls.getSearchForm().waitToBeActive();
        }

        return this;
    }

    public BlogPage searchFor(String query) {
        openSearchForm();
        controls.getSearchForm().getSearchTextInput().clear();
        controls.getSearchForm().getSearchTextInput().sendKeys(query);
        controls.getSearchForm().getSubmitSearchButton().click();

        controls.waitForPageLoad();

        return this;
    }

    public BlogPage cancelSearchWithButton() {
        controls.getSearchForm().getCancelSearchButton().click();

        controls.waitForPageLoad();

        return this;
    }

    public BlogPage cancelSearchLosingFocus() {
        controls.getEmptySpace().click();

        controls.waitForPageLoad();

        return this;
    }

//    Topics-Dropdown actions
    public BlogPage openTopicsDropdown() {
        if (!controls.getTopicsDropdown().isActive()) {
            controls.getTopicsDropdown().getDropdown().click();

            controls.getTopicsDropdown().waitToBeActive();
        }

        return this;
    }

    public BlogPage chooseAllTopics() {
        openTopicsDropdown();
        controls.getTopicsDropdown().getAllTopicsOption().click();

        controls.waitForPageLoad();

        return this;
    }

    public BlogPage chooseRulesTopic() {
        openTopicsDropdown();
        controls.getTopicsDropdown().getRulesOption().click();

        controls.waitForPageLoad();

        return this;
    }

    //    Blog items actions
    public HashSet<String> getFoundTopics() {
        HashSet<String> topics = new HashSet<>();
        for (BlogPageUIMap.BlogItem blogItem : controls.getBlogItems()) {
            topics.add(blogItem.getCategoryLink().getText());
        }

        return topics;
    }
}
