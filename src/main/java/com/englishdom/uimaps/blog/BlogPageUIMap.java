package com.englishdom.uimaps.blog;

import com.englishdom.util.WaitUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class BlogPageUIMap {
    private WebDriver webDriver;

    private static final int DEFAULT_BLOG_ITEMS_COUNT = 12;

    // General css values
    private static final String EMPTY_SPACE = "div.main-wrapper";
    private static final String ACTIVE_ELEMENT_CSS_CLASS = "is-active";
    // Content area css values
    private static final String CONTENT_AREA_CSS = "div.p-blog-main";
    private static final String CONTENT_AREA_LOADING_CSS_CLASS = "is-loading";
    private static final String BLOG_ITEM_CSS = "div.blog-item";
    // preloader
    private static final String PRELOADER_CSS = "div.b-loader.phn-loader";
    private static final String VISIBLE_PRELOADER_CSS_CLASS = ".is-show";
    // filters area
    private static final String DATE_FILTER_CSS = "li.date";
    private static final String RATING_FILTER_CSS = "li.rating";
    private static final String COMMENTS_FILTER_CSS = "li.comment";

    private TopicsDropdown topicsDropdown;
    private SearchForm searchForm;

    @FindBy(how = How.CSS, using = BLOG_ITEM_CSS)
    private List<WebElement> blogItems;

    @FindBy(how = How.CSS, using = EMPTY_SPACE)
    private WebElement emptySpace;

    @FindBy(how = How.CSS, using = CONTENT_AREA_CSS)
    private WebElement contentArea;

    @FindBy(how = How.CSS, using = PRELOADER_CSS)
    private WebElement preloader;

    @FindBy(how = How.CSS, using = DATE_FILTER_CSS)
    private WebElement dateFilter;

    @FindBy(how = How.CSS, using = RATING_FILTER_CSS)
    private WebElement ratingFilter;

    @FindBy(how = How.CSS, using = COMMENTS_FILTER_CSS)
    private WebElement commentsFilter;

    public BlogPageUIMap(final WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    /**
     * Retrieves all blog items on the page and return them in the same order they are displayed.
     *
     * @return  List of BlogItem objects sorted with Comparator based on element location.
     */
    public List<BlogItem> getBlogItems() {
        List<BlogItem> blogs = new ArrayList<>();

        for (WebElement blogItem : blogItems) {
            BlogItem item = new BlogItem(blogItem);
            blogs.add(item);
        }

        blogs.sort(BlogItem.getByLocationComparator());

        return blogs;
    }

    public WebElement getEmptySpace() {
        return emptySpace;
    }

    public WebElement getDateFilter() {
        return dateFilter;
    }

    public WebElement getRatingFilter() {
        return ratingFilter;
    }

    public WebElement getCommentsFilter() {
        return commentsFilter;
    }

    public TopicsDropdown getTopicsDropdown() {
        if (topicsDropdown == null) {
            topicsDropdown = PageFactory.initElements(webDriver, TopicsDropdown.class);
        }

        return topicsDropdown;
    }

    public SearchForm getSearchForm() {
        if (searchForm == null) {
            searchForm = PageFactory.initElements(webDriver, SearchForm.class);
        }

        return searchForm;
    }

//    Waits
    public BlogPageUIMap waitForPageLoad() {
        waitForPreloaderLoad();
        waitForContentLoad();
        WaitUtils.waitForJQueryToLoad(webDriver);

        return this;
    }

    private BlogPageUIMap waitForPreloaderLoad() {
        if (preloader.getAttribute("class").contains(VISIBLE_PRELOADER_CSS_CLASS)) {
            WaitUtils.waitForAttributeValueToDisappear(
                    preloader,
                    "class",
                    VISIBLE_PRELOADER_CSS_CLASS);
        }

        return this;
    }

    private BlogPageUIMap waitForContentLoad() {
        if (contentArea.getAttribute("class").contains(CONTENT_AREA_LOADING_CSS_CLASS)) {
            WaitUtils.waitForAttributeValueToDisappear(
                    contentArea,
                    "class",
                    CONTENT_AREA_LOADING_CSS_CLASS);

        }

        WaitUtils.switchOffImplicitWait(webDriver);

        try {
            webDriver.findElement(By.cssSelector("div.pager-btn"));
            WaitUtils.waitForAllElementsLoaded(BLOG_ITEM_CSS, DEFAULT_BLOG_ITEMS_COUNT);
        } catch (NoSuchElementException ignored) {
            WaitUtils.waitForAttributeToStopChanging(
                    webDriver.findElement(By.cssSelector("section.l-content")), "style");
        }

        WaitUtils.switchOnImplicitWait(webDriver);

        return this;
    }


    public static class TopicsDropdown {
        private static final String DROPDOWN_CSS = "button.tutorial-list-dropdown";
        private static final String ALL_TOPICS_OPTION_CSS = "li[data-alias]";
        private static final String RULES_OPTION_CSS = "li[data-alias=\"pravila\"] > a";

        @FindBy(how = How.CSS, using = DROPDOWN_CSS)
        private WebElement dropdown;

        @FindBy(how = How.CSS, using = ALL_TOPICS_OPTION_CSS)
        private WebElement allTopicsOption;

        @FindBy(how = How.CSS, using = RULES_OPTION_CSS)
        private WebElement rulesOption;

        public WebElement getRulesOption() {
            return rulesOption;
        }

        public WebElement getAllTopicsOption() {
            return allTopicsOption;
        }

        public WebElement getDropdown() {
            return dropdown;
        }

        public boolean isActive() {
            return dropdown.getAttribute("class").contains(ACTIVE_ELEMENT_CSS_CLASS);
        }
        public void waitToBeActive() {
            if (!isActive()) {
                WaitUtils.waitForAttributeValue(dropdown, "class", ACTIVE_ELEMENT_CSS_CLASS);
            }
        }
    }

    public static class SearchForm {
        private static final String SEARCH_FORM_CSS = "form.btn-search-wrap";
        private static final String OPEN_SEARCH_FORM_BUTTON_CSS = "span.btn-search.tutorial-search";
        private static final String SEARCH_TEXT_INPUT_ID = "search";
        private static final String CANCEL_SEARCH_BUTTON_CSS = "span.search-cancel";
        private static final String SUBMIT_SEARCH_BUTTON_CSS = "button.btn-search";

        @FindBy(how = How.CSS, using = SEARCH_FORM_CSS)
        private WebElement searchForm;

        @FindBy(how = How.CSS, using = OPEN_SEARCH_FORM_BUTTON_CSS)
        private WebElement openSearchFormButton;

        @FindBy(how = How.ID, using = SEARCH_TEXT_INPUT_ID)
        private WebElement searchTextInput;

        @FindBy(how = How.CSS, using = CANCEL_SEARCH_BUTTON_CSS)
        private WebElement cancelSearchButton;

        @FindBy(how = How.CSS, using = SUBMIT_SEARCH_BUTTON_CSS)
        private WebElement submitSearchButton;

        public WebElement getSubmitSearchButton() {
            return submitSearchButton;
        }

        public WebElement getCancelSearchButton() {
            return cancelSearchButton;
        }

        public WebElement getSearchTextInput() {
            return searchTextInput;
        }

        public WebElement getOpenSearchFormButton() {
            return openSearchFormButton;
        }

        public boolean isActive() {
            return searchForm.getAttribute("class").contains(ACTIVE_ELEMENT_CSS_CLASS);
        }
        public void waitToBeActive() {
            if (!isActive()) {
                WaitUtils.waitForAttributeValue(searchForm, "class", ACTIVE_ELEMENT_CSS_CLASS);
            }
        }

    }

    public static class BlogItem {
        private static final String BLOG_ITEM_IMG_CSS = "div.blog-item-img";
        private static final String BLOG_ITEM_CATEGORY_LINK_CSS = "a.blog-item-category";
        private static final String BLOG_ITEM_TITLE_CSS = "div.blog-item-title";
        private static final String BLOG_ITEM_DATE_CSS = "div.blog-item-info_date";
        private static final String BLOG_ITEM_LIKES_CSS = "i.i-blog-finger ~ span";
        private static final String BLOG_ITEM_COMMENTS_CSS = "i.i-blog-comment ~ span";

        private WebElement blogItem;

        public BlogItem(final WebElement blogItem) {
            this.blogItem = blogItem;
        }

        public WebElement getImage() {
            return blogItem.findElement(By.cssSelector(BLOG_ITEM_IMG_CSS));
        }

        public WebElement getCategoryLink() {
            return blogItem.findElement(By.cssSelector(BLOG_ITEM_CATEGORY_LINK_CSS));
        }

        public WebElement getTitle() {
            return blogItem.findElement(By.cssSelector(BLOG_ITEM_TITLE_CSS));
        }

        public WebElement getDate() {
            return blogItem.findElement(By.cssSelector(BLOG_ITEM_DATE_CSS));
        }

        public WebElement getLikes() {
            return blogItem.findElement(By.cssSelector(BLOG_ITEM_LIKES_CSS));
        }

        public WebElement getComments() {
            return blogItem.findElement(By.cssSelector(BLOG_ITEM_COMMENTS_CSS));
        }

        private static Comparator<BlogItem> getByLocationComparator() {
            return new Comparator<BlogItem>() {
                @Override
                public int compare(final BlogPageUIMap.BlogItem elementOne, final BlogPageUIMap.BlogItem elementTwo) {
                    if (elementOne == elementTwo) {
                        return 0;
                    }

                    if (elementOne == null) {
                        return -1;
                    }

                    if (elementTwo == null) {
                        return 1;
                    }

                    int elementHeight = elementTwo.getImage().getSize().getHeight();
                    Integer x1 = elementOne.getImage().getLocation().getX();
                    Integer y1 = elementOne.getImage().getLocation().getY();

                    Integer x2 = elementTwo.getImage().getLocation().getX();
                    Integer y2 = elementTwo.getImage().getLocation().getY();

                    if (x1.compareTo(x2) > 0) {
                        if (y1.compareTo(y2) < 0 && Math.abs(y1 - y2) > elementHeight) {
                            return -1;
                        } else {
                            return 1;
                        }
                    } else if (x1.compareTo(x2) < 0) {
                        if (y1.compareTo(y2) > 0 && Math.abs(y1 - y2) > elementHeight) {
                            return 1;
                        } else {
                            return -1;
                        }
                    } else {
                        return y1.compareTo(y2);
                    }
                }
            };
        }
    }
}
