package com.englishdom.webdriver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class WebDriverFactory {
	/* Browsers constants */
	public static final String CHROME = "chrome";
	public static final String FIREFOX = "firefox";
	public static final String INTERNET_EXPLORER = "ie";

	private static WebDriver webDriver = null;

	public static WebDriver getInstance(String browser) {

		if (webDriver != null) {
			return webDriver;
		}

		if (browser.isEmpty()) {
			browser = "firefox";
		}

		if (CHROME.equals(browser)) {
			System.setProperty("webdriver.chrome.driver", "src/main/resources/drivers/chromedriver.exe");
			webDriver = new ChromeDriver();

		} else if (FIREFOX.equals(browser)) {
			webDriver = new FirefoxDriver();

		} else if (INTERNET_EXPLORER.equals(browser)) {
			System.setProperty("webdriver.ie.driver", "src/main/resources/drivers/IEDriverServer.exe");
			webDriver = new InternetExplorerDriver();

		} else {

			throw new RuntimeException("Browser is not currently supported");
		}

		return webDriver;
	}

	public static WebDriver getCurrentDriver() {
		return webDriver;
	}
}