package com.englishdom;

import com.englishdom.pages.EnglishDom;
import com.englishdom.util.AllureOnFailListener;
import com.englishdom.util.PropertyLoader;
import com.englishdom.webdriver.WebDriverFactory;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;

import java.util.concurrent.TimeUnit;

@Listeners({AllureOnFailListener.class})
public class TestBase {
	protected WebDriver driver;

	protected String baseUrl;

	protected String browser;

    protected EnglishDom englishDom;


	@BeforeClass
	public void init() {
		baseUrl = PropertyLoader.loadProperty("site.url");
        if (System.getProperty("site.url") != null) {
            browser = System.getProperty("site.url");
        }

		browser = PropertyLoader.loadProperty("browser.name");
        if (System.getProperty("browser") != null) {
            browser = System.getProperty("browser");
        }

        long implicitWait = Long.parseLong(PropertyLoader.loadProperty("wait.implicit"));

		driver = WebDriverFactory.getInstance(browser);
		driver.manage().timeouts().implicitlyWait(implicitWait, TimeUnit.SECONDS);
		driver.manage().window().maximize();
        englishDom = new EnglishDom(driver);
        driver.get(baseUrl);
	}

	@AfterSuite(alwaysRun = true)
	public void tearDown() {
		if (driver != null) {
			driver.quit();
		}
	}
}
