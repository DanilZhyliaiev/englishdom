package com.englishdom.blog;

import com.englishdom.TestBase;
import com.englishdom.uimaps.blog.BlogPageUIMap;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.Date;

@Features("Blog")
public class BlogSystemTest extends TestBase {
    private final int expectedBlogItemsCount = 12;
    private final String rulesTopicTitle = "Правила";

    @Stories("Sorting")
    @Test
    public void testFilterByRating() {
        englishDom.getBlogPage().filterByRating();

        Assert.assertTrue(englishDom.getBlogPage().getControls().getRatingFilter().
                getAttribute("class").contains("is-active"), "Rating filter is not active.");

        Assert.assertEquals(englishDom.getBlogPage().getBlogItems().size(),
                expectedBlogItemsCount,
                String.format("Blog items count is not %s.", expectedBlogItemsCount));

        Assert.assertTrue(areBlogItemsSortedByRating(),
                "All blog items should be sorted by likes count.");
    }

    @Stories("Topics menu")
    @Test(dependsOnMethods = "testFilterByRating", alwaysRun = true)
    public void testChooseRulesTopic() {
        englishDom.getBlogPage().chooseRulesTopic();

        Assert.assertEquals(englishDom.getBlogPage().getBlogItems().size(),
                expectedBlogItemsCount,
                String.format("Blog items count is not %s.", expectedBlogItemsCount));

        Assert.assertEquals(englishDom.getBlogPage().getFoundTopics().size(), 1,
                String.format("All blog items should have the same topic = '%s'", rulesTopicTitle));

        Assert.assertTrue(englishDom.getBlogPage().getFoundTopics().contains(rulesTopicTitle),
                String.format("All blog items should have the same topic = '%s'", rulesTopicTitle));

        Assert.assertTrue(areBlogItemsSortedByRating(),
                "All blog items should be sorted by likes count.");

    }

    @Stories("Sorting")
    @Test(dependsOnMethods = "testChooseRulesTopic", alwaysRun = true)
    public void testFilterByComments() {
        englishDom.getBlogPage().filterByComments();

        Assert.assertTrue(englishDom.getBlogPage().getControls().getCommentsFilter().
                getAttribute("class").contains("is-active"), "Comment filter is not active.");

        Assert.assertEquals(englishDom.getBlogPage().getBlogItems().size(),
                expectedBlogItemsCount,
                String.format("Blog items count is not %s.", expectedBlogItemsCount));

        Assert.assertEquals(englishDom.getBlogPage().getFoundTopics().size(), 1,
                String.format("All blog items should have the same topic = '%s'", rulesTopicTitle));

        Assert.assertTrue(englishDom.getBlogPage().getFoundTopics().contains(rulesTopicTitle),
                String.format("All blog items should have the same topic = '%s'", rulesTopicTitle));

        Assert.assertTrue(areBlogItemsSortedByComments(),
                "All blog items should be sorted by comments count.");

    }

    @Stories("Topics menu")
    @Test(dependsOnMethods = "testFilterByComments", alwaysRun = true)
    public void testChooseAllTopics() {
        englishDom.getBlogPage().chooseAllTopics();

        Assert.assertTrue(englishDom.getBlogPage().getControls().getCommentsFilter().
                getAttribute("class").contains("is-active"), "Comment filter is not active.");

        Assert.assertEquals(englishDom.getBlogPage().getBlogItems().size(),
                expectedBlogItemsCount,
                String.format("Blog items count is not %s.", expectedBlogItemsCount));


        Assert.assertTrue(englishDom.getBlogPage().getFoundTopics().size() > 1,
                "There must be blog items of different topics on the page");

        Assert.assertTrue(areBlogItemsSortedByComments(),
                "All blog items should be sorted by comments count.");
    }

    @Stories("Sorting")
    @Test(dependsOnMethods = "testChooseAllTopics", alwaysRun = true)
    public void testFilterByDate() throws ParseException {
        englishDom.getBlogPage().filterByDate();

        Assert.assertTrue(englishDom.getBlogPage().getControls().getDateFilter().
                getAttribute("class").contains("is-active"), "Date filter is not active.");

        Assert.assertEquals(englishDom.getBlogPage().getBlogItems().size(),
                expectedBlogItemsCount,
                String.format("Blog items count is not %s.", expectedBlogItemsCount));

        Assert.assertTrue(englishDom.getBlogPage().getFoundTopics().size() > 1,
                "There must be blog items of different topics on the page");

        Assert.assertTrue(areBlogItemsSortedByDate(), "All blog items should be sorted by date.");
    }

    @Stories("Search")
    @Test(dependsOnMethods = "testFilterByDate", alwaysRun = true)
    public void testOpenSearch() {
        String expectedPlaceholder = "поиск";

        englishDom.getBlogPage().openSearchForm();

        Assert.assertTrue(englishDom.getBlogPage().getControls().getSearchForm()
                .getSearchTextInput().equals(driver.switchTo().activeElement()),
                "Search text input must be in focus.");

        Assert.assertEquals(englishDom.getBlogPage().getControls().getSearchForm()
                .getSearchTextInput().getAttribute("placeholder"),
                expectedPlaceholder,
                String.format("Search text input must have placeholder = %s ", expectedPlaceholder));

//        TODO: Assert search panel expand with animation to the left and above filter panel ???
    }

    @Stories("Search")
    @Test(dependsOnMethods = "testOpenSearch", alwaysRun = true)
    public void testSearchForText() {
        String searchQuery = "sweatshops";
        String expectedTitle = "sweatshops";
        int expectedBlogItemsCount = 1;

        englishDom.getBlogPage().searchFor(searchQuery);

        Assert.assertEquals(englishDom.getBlogPage().getControls().getSearchForm()
                .getSearchTextInput().getAttribute("value"),
                searchQuery,
                String.format("Search text input must have value = %s.", searchQuery));

        Assert.assertEquals(englishDom.getBlogPage().getBlogItems().size(),
                expectedBlogItemsCount,
                String.format("Blog items count is not %s.", expectedBlogItemsCount));

        Assert.assertEquals(englishDom.getBlogPage().getControls()
                .getBlogItems().get(0).getTitle().getText(),
                expectedTitle,
                String.format("Found blog item must have title = %s.", expectedTitle));
    }

    @Stories("Search")
    @Test(dependsOnMethods = "testSearchForText", alwaysRun = true)
    public void testCancelSearchWithButton() {
        englishDom.getBlogPage().cancelSearchWithButton();

        Assert.assertFalse(englishDom.getBlogPage().getControls().getSearchForm().isActive(),
                "Search form must be hidden.");

        Assert.assertEquals(englishDom.getBlogPage().getBlogItems().size(),
                expectedBlogItemsCount,
                String.format("Blog items count is not %s.", expectedBlogItemsCount));

        Assert.assertTrue(englishDom.getBlogPage().getFoundTopics().size() > 1,
                "There must be blog items of different topics on the page");
    }

    @Stories("Search")
    @Test(dependsOnMethods = "testCancelSearchWithButton", alwaysRun = true)                        // TODO: reuse first method
    public void testOpenSearchSecondTime() {
        String expectedPlaceholder = "поиск";
        englishDom.getBlogPage().openSearchForm();

        Assert.assertTrue(englishDom.getBlogPage().getControls().getSearchForm().getSearchTextInput()
                .equals(driver.switchTo().activeElement()), "Search text input must be in focus.");

        Assert.assertEquals(englishDom.getBlogPage().getControls().getSearchForm()
                        .getSearchTextInput().getAttribute("placeholder"),
                expectedPlaceholder,
                String.format("Search text input must have placeholder = %s ", expectedPlaceholder));

//        TODO: Assert search panel expand with animation to the left and above filter panel ???
    }

    @Stories("Search")
    @Test(dependsOnMethods = "testOpenSearchSecondTime", alwaysRun = true)
    public void testCloseSearchLosingFocus() {
        englishDom.getBlogPage().cancelSearchLosingFocus();

        Assert.assertFalse(englishDom.getBlogPage().getControls().getSearchForm().isActive(),
                "Search form must be hidden.");
    }


    public boolean areBlogItemsSortedByRating() {
        boolean sortedByLikes = true;
        int previousItemLikesCount = Integer.parseInt(englishDom.getBlogPage()
                .getBlogItems().get(0).getLikes().getText());

        for (BlogPageUIMap.BlogItem blogItem : englishDom.getBlogPage().getBlogItems()) {
            int currentItemLikesCount = Integer.parseInt(blogItem.getLikes().getText());
            if (currentItemLikesCount > previousItemLikesCount) {
                sortedByLikes = false;
                break;
            }

            previousItemLikesCount = currentItemLikesCount;
        }

        return sortedByLikes;
    }

    public boolean areBlogItemsSortedByComments() {
        boolean sortedByComments = true;
        int previousItemCommentsCount = Integer.parseInt(englishDom.getBlogPage()
                .getBlogItems().get(0).getComments().getText());

        for (BlogPageUIMap.BlogItem blogItem : englishDom.getBlogPage().getBlogItems()) {
            int currentItemCommentsCount = Integer.parseInt(blogItem.getComments().getText());
            if (currentItemCommentsCount > previousItemCommentsCount) {
                sortedByComments = false;
                break;
            }

            previousItemCommentsCount = currentItemCommentsCount;
        }

        return sortedByComments;
    }

    public boolean areBlogItemsSortedByDate() throws ParseException {
        boolean sortedByLikes = true;
        DateFormat dateFormat1 = new SimpleDateFormat("dd MMMM yyyy");
        DateFormat dateFormat2 = new SimpleDateFormat("dd MMMM");
        Date previousItemDate;

        try {
            previousItemDate = dateFormat1.parse(englishDom.getBlogPage().getControls()
                    .getBlogItems().get(0).getDate().getText());
        } catch (ParseException e) {
            previousItemDate = dateFormat2.parse(englishDom.getBlogPage().getControls()
                    .getBlogItems().get(0).getDate().getText());
            previousItemDate.setYear(Year.now().getValue());
        }

        for (BlogPageUIMap.BlogItem blogItem : englishDom.getBlogPage().getBlogItems()) {
            Date currentItemDate;
            try {
                currentItemDate = dateFormat1.parse(blogItem.getDate().getText());
            } catch (ParseException e) {
                currentItemDate = dateFormat2.parse(blogItem.getDate().getText());
                currentItemDate.setYear(Year.now().getValue());

            }

            if (currentItemDate.compareTo(previousItemDate) > 0) {
                sortedByLikes = false;
                break;
            }

            previousItemDate = currentItemDate;
        }

        return sortedByLikes;
    }
}
